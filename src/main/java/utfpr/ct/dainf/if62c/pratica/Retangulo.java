/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author samu
 */
public class Retangulo implements FiguraComLados {
    private double base;
    private double altura;
    
    public Retangulo(double base, double altura){
        this.base = base;
        this.altura = altura;
    }
    
    @Override
    public double getLadoMenor(){
        return altura;
    }
    
    @Override
    public double getLadoMaior(){
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    @Override
    public double getPerimetro(){
        return 2*(getLadoMaior() + getLadoMenor());
    }
    
    @Override
    public double getArea(){
        return getLadoMaior() * getLadoMenor();
    }
    
}
