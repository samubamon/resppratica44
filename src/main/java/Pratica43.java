
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Figura;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica43 {
    public static final String FORMAT_MSG = "%s de %s = %g";
    public static void main(String[] args) {
        Figura[] figuras;
        figuras = new Figura[5];
        
        figuras[0] = new Quadrado(1);
        figuras[1] = new TrianguloEquilatero(2);
        figuras[2] = new Retangulo(3, 4);
        figuras[3] = new Elipse(4, 2);
        figuras[4] = new Circulo(3);
        
        for (Figura f : figuras) {
            System.out.println(String.format(FORMAT_MSG, "Área", f, f.getArea()));
            System.out.println(String.format(FORMAT_MSG, "Perímetro", f, f.getPerimetro()));
            System.out.println();
        }
        
    }
}
